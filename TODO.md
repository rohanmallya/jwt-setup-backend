# TODO

## Remaining


* Integrate Social media login(s) if possible
* Separate prescription model to be shareable between Patient and Pharmacist
* Write route for doctor to also get pending appointments and change status

## Done

* Tackle first-name, middle-name and last-name into 3 different fields to improve search.
    /search/<term> can not have spaces. Or use req.body (figure out) 
    *   Solution : all names stored as lowercase in db. Format to lowercase before search
    *   Rohan Mallya becomes rohan+mallya while searching and + is replaced by space in the backend

* Flush database
* Remove dummy functions from auth.controller.js (dummy() and postDummy())
* Remove dummy testing routes from auth.routes.js
* Change sign-in routes to accomodate name and other details of patients and doctors
