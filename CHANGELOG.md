## v0.0.1 -> v0.0.2

*   Successful Generation of Javascript Web Token!
*   User account creation
*   Login works!

## v0.0.2 -> v0.0.3

*   Added models for doctor
*   Separated Logins for User, Doctor.
*   Separated previousVisit and appointment component to be shareable between doctor & patient
*   Tested Data sending capabilities through POST to send Patient-And-Doctor info via Post Routes! (Spoiler : It **works** \m/)

## v0.0.3 -> v0.0.4

*   Cleaned all testing routes
*   Function for patient to search doctor by name added. Returns all matches if multiple doctors with same name present or 404 if none
*   Cleaned doctor model, added hashing and minor changes to Doctor and Patient model.

## v0.0.4 -> v0.0.5

* Patient can now search doctors without case-sensitivity
* Doctor schema changed to accomodate bio, rating, average fees, etc.
* Return from search is changed and sends more data than before
* Signing up a doctor will take in all required data instead of just username and password

## v0.0.5 -> v0.0.6

* Added search functionality for doctor to search patient (might be removed in the future)
* Improved patient signup, takes in more details
* Created Review Model
* Fixed bugs in Prescription Model
* Renamed PreviousVisit model to History Model because PreviousVisit is history (duh)
* Updated MongooseJS version (4.x.x -> 5.2.x) to fix $pushAll conflicts/errors.
* Added route to send all appointment details in Patient.routes. 

## v0.0.6 -> v0.0.7

* Minor changes in appointment model to include additional important fields
* Patient route to cancel an appointment written
* Patient route to create an appointment written

## v0.0.7 -> v0.0.8

*  Added Doctor Route to get pending appointments
*  Added doctor route to accept or reject appointments
*  