var Patient = require("../models/patient.model");
var Doctor = require("../models/doctor.model");
var Appointment = require('../models/appointment.model');
var fs = require('fs');
const JSON = require('circular-json');

// Incomplete
exports.getHistory = async function (req, res, next) {
    var id = req.params.id;
    console.log(id);
    var patient = await Patient.findById(id);
    console.log(patient);
    var history = patient['history'];
    console.log(history);
    var temp = [];
}

exports.searchDoctor = async function (req, res, next) {
    var drName = req.params.name;

    drName = drName.replace("+", " ");
    drName = drName.toLowerCase();
    var doctor = await Doctor.find({
        name: drName
    });
    if (doctor.length == 0) {
        res.json({
            status: 404
        }).status(404);
        return;
    }
    var toSend = [];

    for (var x = 0; x < doctor.length; x++) {
        myDoctor = doctor[x];
        myAdd = myDoctor['addresses'][0];
        tempJSON = {
            id:myDoctor['_id'],
            name: myDoctor['name'],
            speciality: myDoctor['speciality'],
            addresses: myDoctor['addresses'],
            bio: myDoctor['bio'],
            qualifications:myDoctor['qualifications']
        }
        toSend.push(tempJSON);
        // console.log(toSend);
    }
    res.status(201).json(toSend);
}

exports.consultDoctor = async function (req,res,next) {
    // console.log(req);
    // console.log((req.body))
    // console.log(req.body);
    var patient = await Patient.findById(req.user.id);
    var doctor = await Doctor.findById(req.body._id);
    var patientName = patient['name'];
    var patientContact = patient['contactNumber'];
    var status = "Pending";
    var doctorName = doctor['name'];
    var clinicAddress = req.body.address;
    var doctorContact = doctor['contactNumber'];
    var time = req.body.time;
    var date = req.body.date;
    var reasonForVisit = req.body.reasonForVisit;
    var json = {
        patientId:patient['_id'],
        doctorId:doctor['_id'],
        patientName:patientName,
        patientContact:patientContact,
        status:status,
        doctorName:doctorName,
        doctorSpeciality: doctor['speciality'],
        clinicAddress:clinicAddress,
        doctorContact:doctorContact,
        time:time,
        date:date,
        reasonForVisit:reasonForVisit
    }
    var appointment = new Appointment(json);
    var appointment = await appointment.save();
    (patient['appointment']).push(appointment['_id']);
    (doctor['appointment']).push(appointment['_id']);
    var p = await patient.save();
    var d = await doctor.save();
    res.status(201).json(appointment);
}

exports.getAppointments = async function (req,res,next){
    var patient = await Patient.findById(req.user.id);
    var appointments = patient['appointment'];
    var toSend = [];
    for(var x = 0;  x < appointments.length; x++){
        var tempApp = await Appointment.findById(appointments[x]);
        var tempJSON = {
            appointmentId: tempApp['_id'],
            doctorId: tempApp['doctorId'],
            clinicAddress: tempApp['clinicAddress'],
            status : tempApp['status'],
            doctorName : tempApp['doctorName'],
            time: tempApp['time'],
            date: tempApp['date'],
            doctorSpeciality: tempApp['doctorSpeciality'],
            reasonForVisit : tempApp['reasonForVisit']

        }
        toSend.push(tempJSON)
    }
    res.status(201).send(toSend);
}

exports.cancelAppointments = async function (req,res,next){
    var appointment = await Appointment.findById(req.params.appointmentId);
    appointment['status'] = "Cancelled";
    appointment.save();
}