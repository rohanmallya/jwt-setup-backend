var Doctor = require("../models/doctor.model");
var Patient = require("../models/patient.model");
var Appointment = require('../models/appointment.model');
exports.searchUser = async function (req, res, next) {
    var patientName = req.params.name;
    patientName = patientName.replace("+", " ");
    patientName = patientName.toLowerCase();

    var patient = await Patient.find({
        name: patientName
    });
    // console.log(patient);
    if (patient.length == 0) {
        res.json({
            status: 404
        }).status(404);
        return;
    }
    var toSend = [];
    for (var x = 0; x < patient.length; x++) {
        myPatient = patient[x];
        var myJSON = {
            id:myPatient['_id'],
            name:myPatient['name'],
            age:myPatient['age'],
            dob:myPatient['dob']
        }
        toSend.push(myJSON);
        // console.log(toSend);
    }
    res.status(201).json(toSend);
}

exports.getPendingAppointments = async function (req, res, next){
    // console.log(req.user);
    var doctor = await Doctor.findById(req.user.id);
    var appointments = doctor['appointment'];
    var toReturn = [];
    for(var x = 0; x < appointments.length; x++){
        var appointment = await  Appointment.findById(appointments[x]);
        if(appointment['status'] === "Pending"){ // ENUM-IFY
            var tempAppointment = {
                id: appointment['_id'],
                patientName:appointment['patientName'],
                patientContact: appointment['patientContact'],
                clinicAddress:appointment['clinicAddress'],
                time:appointment['time'],
                date:appointment['date'],
                reasonForVisit:appointment['reasonForVisit']
            };
            toReturn.push(tempAppointment);
        }
    }
    res.status(201).json(toReturn);
}

exports.rejectAppointment = async function (req,res, next) {
    var appId = req.params.appId;

    var appointment = await Appointment.findById(appId);
    appointment['status'] = "Cancelled"; //ENUM-IFY
    await appointment.save();

    res.status(201).json(appointment);
}

exports.acceptAppointment = async function(req,res,next){
    var appId = req.params.appId;
    var appointment = await Appointment.findById(appId);
    appointment['status'] = "Accepted"; //ENUM-IFY
    await appointment.save();
    res.status(201).json(appointment);
}