var patientController = require('../controllers/patient.controller'),
    express = require('express'),
    passportService = require('../../config/auth');
    passport = require('passport');

var requireAuth = passport.authenticate('jwt',{session: false});
var requireLogin = passport.authenticate('local', {session:false});
module.exports = (app) => {
  app.get('/:id/history/',patientController.getHistory);
  app.get('/searchdoctor/:name',requireAuth,patientController.searchDoctor);
  app.post('/patient/consult/:doctorId',requireAuth,patientController.consultDoctor);
  app.get('/patient/appointments',requireAuth,patientController.getAppointments);
  app.post('/patient/appointment/:appointmentId/cancel',requireAuth,patientController.cancelAppointments);
}