var drController = require('../controllers/doctor.controller'),
    express = require('express'),
    passportService = require('../../config/auth');
    passport = require('passport');

var requireAuth = passport.authenticate('jwt-doctor',{session: false});
module.exports = (app) => {
    app.get('/searchuser/:name',drController.searchUser);
    app.get('/doctor/pendingAppointments', requireAuth, drController.getPendingAppointments);
    app.post('/doctor/appointment/:appId/reject',requireAuth,drController.rejectAppointment);
    app.post('/doctor/appointment/:appId/accept',requireAuth,drController.acceptAppointment);
    
    // app.get('/doctor/getSchedule',drController.getSchedule);
  }