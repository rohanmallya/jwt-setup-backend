var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var orderJSON = {
    status : String,
    pharmacyName : String,
    pharmacyAddress : String,
    total : Number,
    prescription : {type:mongoose.Schema.Types.ObjectId, ref:'Prescription'}
}

var patientJSON = {
 
    email: {
        type: String,
        lowercase: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name:{type:String, lowercase:true},
    dob: String,
    address: String,
    contactNumber: String,
    prescriptions: [{type:mongoose.Schema.Types.ObjectId, ref:'Prescription'}],
    appointment: [{type:mongoose.Schema.Types.ObjectId, ref:'Appointment'}],
    history : [{type:mongoose.Schema.Types.ObjectId, ref:'History'}],
    orderDetails : [orderJSON]
 
}


var PatientSchema = new mongoose.Schema(patientJSON,{
    timestamps: true
});
 
PatientSchema.pre('save', function(next){
 
    var user = this;
    var SALT_FACTOR = 5;
 
    if(!user.isModified('password')){
        return next();
    }
 
    bcrypt.genSalt(SALT_FACTOR, function(err, salt){
 
        if(err){
            return next(err);
        }
 
        bcrypt.hash(user.password, salt, null, function(err, hash){
 
            if(err){
                return next(err);
            }
 
            user.password = hash;
            next();
 
        });
 
    });
 
});
 
PatientSchema.methods.comparePassword = function(passwordAttempt, cb){
 
    bcrypt.compare(passwordAttempt, this.password, function(err, isMatch){
 
        if(err){
            return cb(err);
        } else {
            cb(null, isMatch);
        }
    });
 
}
 
module.exports = mongoose.model('Patient', PatientSchema);