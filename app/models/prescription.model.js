const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var medicineJSON = {
    name : String,
    instruction : [Number],
    directions : String,
    duration: Number,
}
var testJSON = {
    name: String,
    comments: String
}
var prescriptionJSON = {
    patientID : Schema.Types.ObjectId,
    patientName: String,
    patientDOB : String,
    date : String,
    doctorName : String,
    doctorQualitifaction : String,
    clinicAddress : String,
    doctorContact : String,
    medicines : [medicineJSON],
    tests : [testJSON],
    status: String,
}

/*
    var prescriptionJSON = {
        patientID : 0813,
        doctorID : 12087423,
        ClinicAddress: []
        medicines : [
            {
                name: aoiher, instructions: "Dont take", howMany:5/day, duration: 1 week, reason: "u dying"
            },
            {
                name: poison, instructions:"Take", howMany:1/day, duration: 2 weeks, reason: "Life sucks"
            }
        ]
        tests: [
            {
                name:"CT Scan", comments: "U need it"
            }
        ]
    }
*/
var prescriptionSchema = new mongoose.Schema(prescriptionJSON);
module.exports = mongoose.model('Prescription', prescriptionSchema);