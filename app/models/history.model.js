var mongoose = require('mongoose');


var historyJSON = {
    patientName : String,
    patientContact : String,
    status : String,
    doctorName : String,
    doctorContact : String,
    doctorQualitifaction : String,
    clinicAddress : String,
    time : String,
    date : String,
    reasonForVisit : String,
    prescription: {type:mongoose.Schema.Types.ObjectId, ref:'Prescription'}
}

var historySchema = new mongoose.Schema(historyJSON);
module.exports = mongoose.model('History', historySchema);